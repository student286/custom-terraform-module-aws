bucket_name = "python-app-feb-bucket"
name        = "environment"
environment = "dev-1"

vpc_cidr             = "10.0.0.0/16"
vpc_name             = "dev-proj-eu-central-vpc-1"
cidr_public_subnet   = ["10.0.1.0/24", "10.0.2.0/24"]
cidr_private_subnet  = ["10.0.3.0/24", "10.0.4.0/24"]
eu_availability_zone = ["eu-central-1a", "eu-central-1b"]

public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDL/otmGFJ7q2+ymE4u7K6pUlXqO8omL6IuPkl2d66Nl5PRVtN/SOKWT3/omR8JnSV8IiNDsMLroZmuXjO+dTP4Z9Sg1TIZimVsrNZ8GxLY9HcTtjBjqKhf/Ot1N6REmabT52/u94WWVm5hD3Bizze4WW73WOCUo6lIarf0BEY+E0cmdhDs7hbdX869cxDZuh8lPs2j2MBI/fhPbe/W6mJTu5ErNC/FE5EmdVa9Qz4m0uJJVZwu6SJyeJtSvKP0Qs6hyz/1amig2eRzavX282sxT5L+J8IXy1SM/39whDh8Qxz3d6e1Jw10cmPH6YA/s5uZh0q9hbPskXeRIm7wQvyVinSYjBw4r6JTpOBCcIYlk1vtxB7m1ao7hBq1gjsQ3U610Cm0JzzOna7arMCsbmOmn+agMz+MKtZx0A8xLTtD15gNBxFRuYIwdY/fbJqMKiHMvVYIKJE1ep3/zGjEYhwhzUh42FEWCBuXryfdBwl0f8YK6ZzZDp+AxalzNi21U3c= thaku@Prashant"
ec2_ami_id = "ami-0faab6bdbac9486fb"

ec2_user_data_install_apache = ""

domain_name = "helloprashant.xyz"