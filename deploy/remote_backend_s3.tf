terraform {
  backend "s3" {
    bucket = "python-app-feb-bucket"
    key    = "devops-project-1/terraform.tfstate"
    region = "eu-central-1"
  }
}